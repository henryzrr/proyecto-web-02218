const path = require('path');
const express = require('express'); //importa express
const morgan = require('morgan');
const mongoose = require('mongoose');
const passport = require('passport');
const session = require('express-session');
const cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
const flash = require('connect-flash');

const app = express(); //inicializa express

//conectando a la base de datos
mongoose.connect('mongodb://localhost:27017/crud-web',{ useNewUrlParser: true })
    .then( db => console.log('DB conected'))
    .catch(err => console.log(err));

require('./passport/local-auth');

//importando rutas
var indexRoutes = require('./routes/index');
var positionRoutes = require('./routes/position');
var departmentRoutes = require('./routes/department');
var studenstRoutes = require('./routes/student');
var courseRoutes = require('./routes/course');
var staffRoutes = require('./routes/staff');
var courseProfesor = require('./routes/courseProfesor');
var staffAssignment = require('./routes/staffAssignment');
var enrollment = require('./routes/enrollment');
var certified = require('./routes/certified');
//settings
app.set('port', process.env.PORT || 4000);
app.set('views', path.join(__dirname, 'views')); 
app.set('view engine','pug');
app.use(express.static(__dirname +'/public'));
//middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}));
app.use(bodyParser.urlencoded({extended: false}));

//session
app.use(session({
    secret: 'sesionsecreta',
    resave: false,
    saveUninitialized: true 
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

app.use((req,res,next)=>{
    app.locals.signupMessage=req.flash('signupMessage');
    app.locals.signinMessage=req.flash('signinMessage');
    app.locals.user=req.user;
    next();
});

//rutas
app.use('/certified',certified);
app.use('/enrollment',enrollment);
app.use('/position',positionRoutes);
app.use('/department',departmentRoutes);
app.use('/course', courseRoutes);
app.use('/student', studenstRoutes);
app.use('/staff', staffRoutes);
app.use('/courseProfesor', courseProfesor);
app.use('/staffAssignment',staffAssignment);
app.use('/',indexRoutes);

//esto inicia el servidor 
app.listen(app.get('port'), ()=> {
    console.log(`Server on port ${app.get('port')}`);
});