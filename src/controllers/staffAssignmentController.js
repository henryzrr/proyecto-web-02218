const StaffAssignment = require('../models/staffAssignment');
const Staff = require('../models/staff');
const Position = require('../models/position');
const Department = require('../models/department');
exports.show = async (req, res)=>{
    const staffAssignment = await StaffAssignment.find().populate([{path:'staff_id', populate:{path:'staff_user_id'}},{path:'department_id'},{path: 'position_id'}]);
    //console.log(staffAssignment);
    res.render('staffAssignment/show',{staffAssignment});
};
exports.create = async (req, res) =>{
     const staff = await Staff.find().populate({path:'staff_user_id'});
     const position = await Position.find();
     const department = await Department.find();
     res.render('staffAssignment/create', {staff,position,department});
};
exports.showOptions = async (req, res) =>{
    const staffAssignment = await StaffAssignment.find().populate([{path:'staff_id', populate:{path:'staff_user_id'}},{path:'department_id'},{path: 'position_id'}]);
    res.render('staffAssignment/showOptions',{staffAssignment});
};

exports.createPost = async (req, res)=>{
    const staffAssignment = new StaffAssignment(req.body);
    await staffAssignment.save();
    res.redirect('/staffAssignment/show');
};

exports.editStaffAssignment = async (req,res)=>{
    const staff = await Staff.find().populate({path:'staff_user_id'});
    const departments = await Department.find();
    const positions = await Position.find();
    const { id } = req.params;
    const staffAssignment = await StaffAssignment.find({_id:id}).populate([{path:'staff_id', populate:{path:'staff_user_id'}},{path:'department_id'},{path: 'position_id'}]);
    res.render('staffAssignment/editStaffAssignment', { staff,departments,positions,staffAssignment } );
};
exports.delete = async (req, res)=>{
    const { id } = req.params;
    await StaffAssignment.deleteOne({_id: id});
    res.redirect('../showOptions');
};
exports.editPost = async (req, res)=>{
    //console.log(req.body);
    const id = req.body._id;
    await StaffAssignment.updateOne({_id: id}, req.body);
    res.redirect('showOptions'); 
};