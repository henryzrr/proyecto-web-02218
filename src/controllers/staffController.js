const Staff = require('../models/staff');
const User = require('../models/user');


exports.create = (req, res) =>{
    res.render('staff/create');
};
exports.showOptions = async (req, res) =>{
    const staff= await Staff.find().populate({path:'staff_user_id'});
    console.log(staff);
    res.render('staff/showOptions', {staff});
};

exports.createPost = async (req, res)=>{
    const user = new User(req.body);
    await user.save(function () {
      
        var staff = new Staff({
            staff_user_id: user._id,
            prefix:  req.body.prefix 
        });
        staff.save();
      });
    res.redirect('/staff/showOptions');
};

exports.editStaff = async (req,res)=>{
    const { id } = req.params;
    const staff = await Staff.findById(id).populate({path:'staff_user_id'});;
    //console.log(staffs);
    res.render('staff/editStaff', { staff } );
};
exports.delete = async (req, res)=>{
    const { id } = req.params;
    await Staff.deleteOne({staff_user_id: id});
    await User.deleteOne({_id: id});
    res.redirect('../showOptions');
};
exports.editPost = async (req, res)=>{
    const id = req.body._id;
    await Staff.updateOne({staff_user_id: id}, {prefix: req.body.prefix});
    await User.updateOne({_id: id}, req.body);
    res.redirect('showOptions'); 
};