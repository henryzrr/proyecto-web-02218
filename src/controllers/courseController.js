const Course = require('../models/course');

exports.create = (req, res) =>{
    res.render('course/create');
};
exports.showOptions = async (req, res) =>{
    const courses= await Course.find();
    res.render('course/showOptions', {courses});
};

exports.createPost = async (req, res)=>{
    const course = new Course(req.body);
    await course.save();
    res.redirect('/course/showOptions');
};

exports.editcourse = async (req,res)=>{
    const { id } = req.params;
    const courses = await Course.findById(id);
    res.render('course/editCourse', { courses } );
};
exports.delete = async (req, res)=>{
    const { id } = req.params;
    await Course.deleteOne({_id: id});
    res.redirect('../showOptions');
};
exports.editPost = async (req, res)=>{
    const id = req.body._id;
    await Course.updateOne({_id: id}, req.body);
    res.redirect('showOptions'); 
};