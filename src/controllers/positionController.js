const Position = require('../models/position');

exports.create = async function (req, res) {
    const position = new Position(req.body);
    await position.save();
    res.redirect('../position/show');
};
exports.show = async (req, res) =>{
    const positions = await Position.find();
    console.log(positions);
    res.render('position/show.pug', {positions});
};
exports.showOptions = async (req, res) =>{
    const positions = await Position.find();
    res.render('position/showOptions.pug', {positions});
};
exports.delete = async (req, res) =>{
    const { id } = req.params;
    await Position.deleteOne({_id: id});
    res.redirect('../showOptions');
};
exports.editPosition = async (req, res) =>{
    const { id } = req.params;
    const positions = await Position.findById(id);
    res.render('position/editPosition.pug', { positions });
};
exports.edit = async(req, res) =>{
    const id = req.body._id;
    await Position.updateOne({_id: id}, req.body);
    res.redirect('showOptions');
};