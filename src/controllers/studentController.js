const Student = require('../models/student');
const User = require('../models/user');


exports.create = (req, res) =>{
    res.render('student/create');
};
exports.showOptions = async (req, res) =>{
    const students= await Student.find().populate({path:'student_user_id'});
    //console.log(students)
    res.render('student/showOptions', {students});
};

exports.createPost = async (req, res)=>{
    const user = new User(req.body);
    user.password = user.encryptPassword(req.body.password);
    await user.save(function () {
      
        var student = new Student({
            student_user_id: user._id 
        });
      
        student.save();
      });
    res.redirect('/student/showOptions');
};

exports.editStudent = async (req,res)=>{
    const { id } = req.params;
    const students = await Student.findById(id).populate({path:'student_user_id'});;
    //console.log(students);
    res.render('student/editStudent', { students } );
};
exports.delete = async (req, res)=>{
    const { id } = req.params;
    await Student.deleteOne({student_user_id: id});
    await User.deleteOne({_id: id});
    res.redirect('../showOptions');
};
exports.editPost = async (req, res)=>{
    const id = req.body._id;
    await User.updateOne({_id: id}, req.body);
    res.redirect('showOptions'); 
};