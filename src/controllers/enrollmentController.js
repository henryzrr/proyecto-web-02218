const Enrollment = require('../models/enrollment');
const CourseProfesor = require('../models/courseProfesor');
const User = require('../models/user');
const Certified = require('../models/certified');
const Student = require('../models/student');
exports.show = async (req, res)=>{
    const courses = await CourseProfesor.find().populate({path: 'course_id'});
    //console.log(courses);
    res.render('enrollment/show',{courses});
};
exports.create = async (req, res) =>{
     const courses = await CourseProfesor.find().populate({path: 'course_id'});
     res.render('enrollment/create', {courses});
};
exports.showOptions = async (req, res) =>{
    const courses = await Enrollment.find({course_id:req.body.course_id}).populate([{path:'student_id', populate:{path:'student_user_id'}},{path:'course_id', populate:[{path:'course_id'},{path:'staff_profesor_user_id',populate:{path:'staff_user_id'}}]}]);
    console.log(courses);
    res.render('enrollment/showOptions',{courses});
};

exports.createPost = async (req, res)=>{
    const student = await Student.find().populate({path:'student_user_id',match:{user_id:req.body.ci}}).exec();
    console.log(student);
    const enrollment = new Enrollment();
    enrollment.course_id = req.body.course_id;
    student.forEach(val => {
        if(val.student_user_id!=null){
            enrollment.student_id = val._id;
        }
    });
    await enrollment.save();
    
    res.redirect('/enrollment/show');
};
exports.presenceEnrollment = async (req,res)=>{
    console.log("###################################");
    console.log(req.body._id);
    console.log(req.body.asistencia);
    console.log(req.body.nota);
    console.log("###################################");
    await Enrollment.updateOne({_id:req.body._id},{presence:req.body.presence});
    await Certified.deleteOne({enrollment_id:req.body._id});
    if(req.body.presence >= req.body.minimal_assistance){
        var certified = new Certified();
        certified.enrollment_id = req.body._id;
        certified.emision_date = new Date();
        await certified.save();
    }
    res.redirect('/certified/show');
};

exports.delete = async (req, res)=>{
    const { id } = req.params;
    await Enrollment.deleteOne({_id: id});
    res.redirect('../show');
};
exports.admin = async (req, res)=>{
    const { id } = req.params;
    var estudiante =await Enrollment.find({'_id':id} ).populate([{path:'student_id', populate:{path:'student_user_id'}},{path:'course_id',populate:{path:'course_id'}}]).exec();
    console.log(estudiante);
    res.render('enrollment/admin',{estudiante});
};
exports.agregarNotas = async (req,res) =>{
    const { id } = req.params;
    const alumnos = await Enrollment.find({'course_id':id} ).populate([{path:'student_id', populate:{path:'student_user_id'}},{path:'course_id',populate:{path:'course_id'}}]).exec();
    var asistencia = alumnos[0].course_id.course_id.minimal_assistance;
    var nota =  alumnos[0].course_id.course_id.minimal_score;
    
    if(asistencia==null&&nota==null){
        alumnos.forEach (async alumno => {
            var certified = new Certified();
            certified.enrollment_id = alumno._id;
            certified.emision_date = new Date();
            await certified.save();
            await Enrollment.updateMany({course_id: id}, {have_certifies:true});
            res.redirect('/certified/show'); 
        });
        
    }
    console.log(asistencia);
    res.render('enrollment/final_scores',{alumnos});
};
exports.generarCertificados = async (req,res,next) =>{
    var ls = Object.keys(req.body);
    var tam = ls.length/4;
    console.log(req.body);
    for (let index = 0; index < tam; index++) {
        var id= req.body['lista['+index+'][_id]'];
        var nota = req.body['lista['+index+'][final_score]'];
        var asistencia_minima = req.body['lista['+index+'][minimal_assistance]'];
        var asistencia = req.body['lista['+index+'][presence]'];
        var nota_minima = req.body['lista['+index+'][minimal_score]'];
        var nota = req.body['lista['+index+'][final_score]'];
        console.log(id + " "+ nota+ " "+ asistencia_minima + " "+ asistencia);
        console.log(nota);
        
        if(asistencia_minima!=undefined && nota_minima!=undefined){
            if(asistencia!=undefined && nota!=undefined){
                if(asistencia_minima <= asistencia && nota>=nota_minima){
                    var certified = new Certified();
                    certified.enrollment_id = id;
                    certified.emision_date = new Date();
                    await certified.save();
                }
                await Enrollment.updateOne({_id: id}, {have_certifies:true, presence:asistencia,final_score:nota});
            }else{
                await Enrollment.updateOne({_id: id}, {have_certifies:true, presence:'0',final_score:'0'});
            }
            
        }
        if(nota_minima!=undefined){
            if(nota!=undefined){
                if(nota>=nota_minima){
                    var certified = new Certified();
                    certified.enrollment_id = id;
                    certified.emision_date = new Date();
                    await certified.save();
                }
                await Enrollment.updateOne({_id: id}, {have_certifies:true, final_score:nota});
            }else{
                await Enrollment.updateOne({_id: id}, {have_certifies:true, final_score:'0'});
            }
            
        }
        if(asistencia_minima!=undefined){
            if(asistencia!=undefined){
                if(asistencia_minima <= asistencia){
                    var certified = new Certified();
                    certified.enrollment_id = id;
                    certified.emision_date = new Date();
                    await certified.save();
                }
                await Enrollment.updateOne({_id: id}, {have_certifies:true, presence:asistencia});
            }else{
                await Enrollment.updateOne({_id: id}, {have_certifies:true, presence:'0'});
            }
           
        }
        
    }
    
    return res.send(true);
};