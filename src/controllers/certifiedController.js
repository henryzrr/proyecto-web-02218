const CourseProfesor = require('../models/courseProfesor');
const Enrollment = require('../models/enrollment');
const Certified = require('../models/certified');
exports.show = async (req, res)=>{
    const courses = await Enrollment.find().populate({path:'course_id',populate:{path:'course_id'}});
    //console.log(courses);
    res.render('certified/show',{courses});
};
exports.showOptions = async (req, res) =>{
   // console.log(req.body.course_id);
    const certifies = await Certified.find().populate({path:'enrollment_id',populate:[{path:'student_id', populate:{path:'student_user_id'}},{path:'course_id',populate:{path:'course_id'},match:{_id:req.body.course_id}}]});
    console.log(certifies);
    var course_name = req.body.course_name;
    //console.log({course_name});
    res.render('certified/showOptions',{certifies,course_name});
};
