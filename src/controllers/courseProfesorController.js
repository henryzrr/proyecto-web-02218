const CourseProfesor = require('../models/courseProfesor');
const Staff = require('../models/staff');
const Course = require('../models/course');
exports.show = async (req, res)=>{
    const courses = await CourseProfesor.find().populate([{path:'staff_profesor_user_id', populate:{path:'staff_user_id'}},{path:'course_id'}]);
    res.render('courseProfesor/show',{courses});
};
exports.create = async (req, res) =>{
     const profesors = await Staff.find().populate({path:'staff_user_id'});
     const areas = await Course.find();
     res.render('courseProfesor/create', {profesors,areas});
};
exports.showOptions = async (req, res) =>{
    const courses = await CourseProfesor.find().populate([{path:'staff_profesor_user_id', populate:{path:'staff_user_id'}},{path:'course_id'}]);
    console.log(courses[0].start_date);
    res.render('courseProfesor/showOptions',{courses});
};

exports.createPost = async (req, res)=>{
    const courseProfesor = new CourseProfesor(req.body);
    await courseProfesor.save();
    res.redirect('/courseProfesor/show');
};

exports.editCourseProfesor = async (req,res)=>{
    const profesors = await Staff.find().populate({path:'staff_user_id'});
    const { id } = req.params;
    const courses = await CourseProfesor.find({_id:id}).populate([{path:'staff_profesor_user_id', populate:{path:'staff_user_id'}},{path:'course_id'}]);
    res.render('courseProfesor/editCourseProfesor', { courses,profesors } );
};
exports.delete = async (req, res)=>{
    const { id } = req.params;
    await CourseProfesor.deleteOne({_id: id});
    res.redirect('../showOptions');
};
exports.editPost = async (req, res)=>{
    const id = req.body._id;
    await CourseProfesor.updateOne({_id: id}, req.body);
    res.redirect('showOptions'); 
};