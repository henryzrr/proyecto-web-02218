const department = require('../models/department');

exports.show = async (req, res) =>{
    const departments= await department.find();
    res.render('department/show.pug', {departments});
};
exports.create = (req, res) =>{
    res.render('department/create');
};
exports.showOptions = async (req, res) =>{
    const departments= await department.find();
    res.render('department/showOptions', {departments});
};

exports.createPost = async (req, res)=>{
    const Department = new department(req.body);
    await Department.save();
    res.redirect('/department/show');
};

exports.editDepartment = async (req,res)=>{
    const { id } = req.params;
    const departments = await department.findById(id);
    res.render('department/editDepartment', { departments } );
};
exports.delete = async (req, res)=>{
    const { id } = req.params;
    await department.deleteOne({_id: id});
    res.redirect('../showOptions');
};
exports.editPost = async (req, res)=>{
    const id = req.body._id;
    await department.updateOne({_id: id}, req.body);
    res.redirect('showOptions'); 
};