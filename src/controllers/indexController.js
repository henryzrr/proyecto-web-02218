const passport = require('passport');

exports.index= (req,res,next)=>{
    res.render('index');    
};
exports.signinget= (req,res,next)=>{
    res.render('login');
};
exports.signinpost= passport.authenticate('local-signin',{
    successRedirect:'/home',
    failureRedirect: '/',
    passReqToCallback: true
});
exports.home = (req, res,next)=>{
    res.render('home');
};
exports.logout = (req,res,next)=>{
    req.logout();
    res.redirect('/');
};
