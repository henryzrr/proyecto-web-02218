$(document).ready(function () {
    $("#guardarNotas").click(function (e) { 
        e.preventDefault();
        var lista =[];
        var vacio = false;
        $("tbody tr").each(function () {
            var id = $(this).attr('id');
            var asistencia_minima = $("#"+id+ " input[name='minimal_assistance']").val();
            var asistencia = $("#"+id+ " input[name='presence']").val();
            var puntaje_final = $("#"+id+ " input[name='final_score']").val();
            var nota_minima = $("#"+id+ " input[name='minimal_score']").val();
            var nota = {'_id':id,'minimal_assistance':asistencia_minima,'final_score':puntaje_final,'presence':asistencia,'minimal_score':nota_minima};
            lista.push(nota);
            if(asistencia_minima=="" || puntaje_final==""){
                vacio=true;
            }
        });
        var confirmado;
        if(vacio==true){
            var confirmado = confirm("Existen campos vacios, desea continuar?");
        }else{
            enviarDatos();
        }
        if(confirmado){
            enviarDatos();
        }
        function enviarDatos(){
            $.ajax({
                type: "POST",
                url: "../generarCertificados",
                data: {'lista':lista},
                success: function (response) {
                   if(response==true){
                        window.location.href = "/certified/show"
                   }
                }
            });
        }
    });
});