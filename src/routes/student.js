const express = require('express');
const router = express.Router();
const studentController = require('../controllers/studentController');

router.get('/create', studentController.create);
router.get('/showOptions', studentController.showOptions);
router.get('/editStudent/:id', studentController.editStudent);
router.get('/delete/:id', studentController.delete);

router.post('/create',studentController.createPost);
router.post('/editStudent', studentController.editPost);
module.exports=router;