const express = require('express');
const router = express.Router();
const courseProfesorController = require('../controllers/courseProfesorController');

router.get('/show', courseProfesorController.show);
router.get('/create', courseProfesorController.create);
router.get('/showOptions', courseProfesorController.showOptions);
router.get('/edit/:id', courseProfesorController.editCourseProfesor);
router.get('/delete/:id', courseProfesorController.delete);

router.post('/create',courseProfesorController.createPost);
router.post('/editPost', courseProfesorController.editPost);

module.exports = router;