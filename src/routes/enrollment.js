const express = require('express');
const router = express.Router();
const enrollmentController = require('../controllers/enrollmentController');

router.get('/show', enrollmentController.show);
router.get('/create', enrollmentController.create);
router.post('/showOptions', enrollmentController.showOptions);
router.post('/edit', enrollmentController.presenceEnrollment);
router.get('/delete/:id', enrollmentController.delete);
router.get('/notas/:id',enrollmentController.agregarNotas);
router.get('/admin/:id',enrollmentController.admin);

router.post('/generarcertificados',enrollmentController.generarCertificados);

router.post('/create',enrollmentController.createPost);


module.exports = router;