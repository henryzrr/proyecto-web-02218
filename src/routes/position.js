const express = require('express');
const router = express.Router();
const positionController = require('../controllers/positionController');


router.get('/show',positionController.show);
router.get('/create',(req,res)=>{
    res.render('position/create.pug');
});

router.post('/create', positionController.create);
router.get('/showOptions', positionController.showOptions);
router.get('/delete/:id',positionController.delete);
router.get('/editPosition/:id', positionController.editPosition);
router.post('/editPosition', positionController.edit);
module.exports=router;