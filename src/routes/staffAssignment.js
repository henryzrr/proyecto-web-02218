const express = require('express');
const router = express.Router();
const staffAssignment = require('../controllers/staffAssignmentController');

router.get('/show', staffAssignment.show);
router.get('/create', staffAssignment.create);
router.get('/showOptions', staffAssignment.showOptions);
router.get('/edit/:id', staffAssignment.editStaffAssignment);
router.get('/delete/:id', staffAssignment.delete);

router.post('/create',staffAssignment.createPost);
router.post('/editPost', staffAssignment.editPost);

module.exports = router;