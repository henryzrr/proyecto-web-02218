const express = require('express');
const router = express.Router();
const departmentController = require('../controllers/departmentController');

router.get('/show', departmentController.show);
router.get('/create', departmentController.create);
router.get('/showOptions', departmentController.showOptions);
router.get('/editDepartment/:id', departmentController.editDepartment);
router.get('/delete/:id', departmentController.delete);

router.post('/create',departmentController.createPost);
router.post('/editDepartment', departmentController.editPost);
module.exports=router;