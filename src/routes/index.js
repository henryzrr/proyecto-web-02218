const express = require('express');
const router = express.Router();

router.get('/',(req,res)=>{
    res.render('index.pug');
});
const indexController = require('../controllers/indexController');

router.get('/',indexController.index);
router.get('/signin',indexController.signinget);
router.post('/signin',indexController.signinpost);
router.get('/logout',indexController.logout)
router.get('/home',isAuth, indexController.home);


function isAuth(req,res,next){
    if(req.isAuthenticated()){
        return next();
    }
    res.redirect('/');
}
module.exports = router;