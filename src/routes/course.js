const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseController');

router.get('/create', courseController.create);
router.get('/showOptions', courseController.showOptions);
router.get('/editcourse/:id', courseController.editcourse);
router.get('/delete/:id', courseController.delete);

router.post('/create',courseController.createPost);
router.post('/editcourse', courseController.editPost);
module.exports=router;