const express = require('express');
const router = express.Router();
const staffController = require('../controllers/staffController');

router.get('/create', staffController.create);
router.get('/showOptions', staffController.showOptions);
router.get('/editStaff/:id', staffController.editStaff);
router.get('/delete/:id', staffController.delete);

router.post('/create',staffController.createPost);
router.post('/editStaff', staffController.editPost);
module.exports=router;