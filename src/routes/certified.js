const express = require('express');
const router = express.Router();
const certifiedController = require('../controllers/certifiedController');

router.get('/show', certifiedController.show);
router.post('/showOptions', certifiedController.showOptions);

module.exports = router;