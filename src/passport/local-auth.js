
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/user');

passport.serializeUser((user,done)=>{
    done(null,user.id);
});
passport.deserializeUser(async (id,done)=>{
    const user = await User.findById(id);
    done(null,user);
});

passport.use('local-signin', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
  }, async (req, email, password, done) => {
    const user = await User.findOne({'username': email});
    console.log(password);

    if(!user) {
      return done(null, false, req.flash('signinMessage', 'EL usuario no existe'));
    }
    if(!user.validatePassword(password)) {
      return done(null, false, req.flash('signinMessage', 'Contraseña incorrecta, intente nuevamente'));
    }
    return done(null, user);
  }));

