const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const staffAssignmentSchema = new Schema({
    year: Number,
    department_id:{type: Schema.Types.ObjectId, ref:'departmentSchema'},
    position_id: {type: Schema.Types.ObjectId, ref:'positionSchema'},
    staff_id: {type: Schema.Types.ObjectId, ref: 'staffSchema'},
    start_date: Date,
    finish_date: Date
});

module.exports = mongoose.model('staffAssignmentSchema',staffAssignmentSchema);