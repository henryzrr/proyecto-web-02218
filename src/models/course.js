const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const courseSchema= new Schema({
    course_id: String,
    course_name: String,
    academic_hours: Number,
    minimal_assistance: Number,
    minimal_score: Number
});

module.exports=mongoose.model('courseSchema',courseSchema);