const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const positionSchema = new Schema({
    position_id: String,
    position_name: String
});

module.exports=mongoose.model('positionSchema',positionSchema);