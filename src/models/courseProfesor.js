const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const courseProfesorSchema = new Schema({
    staff_profesor_user_id: {type: Schema.Types.ObjectId, ref: 'staffSchema'},
    course_id: {type: Schema.Types.ObjectId, ref: 'courseSchema'},
    start_date: Date,
    finish_date: Date
});

module.exports = mongoose.model('courseProfesorSchema',courseProfesorSchema);