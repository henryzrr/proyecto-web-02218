const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');

const Schema = mongoose.Schema;

const userSchema = new Schema({
    user_id : Number,
    user_full_name: String,
    username: String,
    password: String
});

userSchema.methods.encryptPassword = function(password){
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
};
userSchema.methods.validatePassword =function (password){
    return bcrypt.compareSync(password,this.password);
};

module.exports = mongoose.model('userSchema',userSchema);

