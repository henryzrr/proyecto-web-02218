const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const departmentSchema= new Schema({
    department_id: String,
    department_name: String
});

module.exports=mongoose.model('departmentSchema',departmentSchema);