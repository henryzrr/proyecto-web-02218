const moongose = require('mongoose');
const Schema = moongose.Schema;

const staffSchema = new Schema({
    staff_user_id: {type: Schema.Types.ObjectId, ref: 'userSchema'},
    prefix: String
});

module.exports = moongose.model('staffSchema', staffSchema);