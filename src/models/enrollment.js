const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const enrollmentSchema = new Schema({
    course_id: {type: Schema.Types.ObjectId, ref: 'courseProfesorSchema'},
    student_id: {type: Schema.Types.ObjectId, ref: 'studentSchema'},
    presence: Number,
    final_score: Number,
    have_certifies: {type: Boolean, default: false}
});

module.exports = mongoose.model('enrollment',enrollmentSchema);