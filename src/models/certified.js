const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const certifiedSchema = new Schema({
    enrollment_id: {type: Schema.Types.ObjectId, ref: 'enrollment'},
    emision_date: Date
});

module.exports = mongoose.model('certified',certifiedSchema);