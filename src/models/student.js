const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const studentSchema = new Schema({
    student_user_id: {type: Schema.Types.ObjectId, ref: 'userSchema'}
});
module.exports = mongoose.model('studentSchema',studentSchema);